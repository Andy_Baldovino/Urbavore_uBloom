uBloom
A companion iOS app for Urbavore's uBloom system.



Acknowledgments

I would like to thank Urbavore (https://growurbavore.com/) for their support in
this project.

I would like to thank both the 2017 and 2018 senior design teams for their help
in this project.



License

Copyright 2018 Andy Baldovino

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
A copy of the License is provided (see LICENSE.txt).

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "as is" basis,
without warranties or conditions of any kind, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

This software uses third-party components which are distributed under their own
terms (see LICENSE_3RD_PARTY.txt).
