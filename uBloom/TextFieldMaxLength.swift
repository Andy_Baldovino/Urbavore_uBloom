//
//  TextFieldMaxLength.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-04-21.
//  Adapted from http://www.globalnerdy.com/2016/05/18/ios-programming-trick-how-to-use-xcode-to-set-a-text-fields-maximum-length-visual-studio-style/
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import UIKit

private var maxLengths = [UITextField: Int]()

extension UITextField {

	@IBInspectable var maxLength: Int {
		get {
			guard let length = maxLengths[self] else {
				return Int.max
			}

			return length
		}

		set {
			maxLengths[self] = newValue
			addTarget(self, action: #selector(limitLength), for: UIControlEvents.editingChanged)
		}

	}

	@objc func limitLength(textField: UITextField) {
		guard let prospectiveText = textField.text, prospectiveText.count > maxLength else {
			return
		}

		let selection = selectedTextRange
		let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
		text = String(prospectiveText[..<maxCharIndex])
		selectedTextRange = selection
	}

}
