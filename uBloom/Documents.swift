//
//  Documents.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-05-29.
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import Foundation

/// Possible errors when reading or writing data.
enum DocumentsError: Error {
	case readError(url: URL)
	case writeError(url: URL)
	case writeErrorWhileClearing(url: URL)
}

//MARK: - Constants

private let documentsUrl = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
let recordsUrl = documentsUrl.appendingPathComponent("records.txt")
let errorsUrl = documentsUrl.appendingPathComponent("errors.txt")

let licenseUrl = Bundle.main.url(forResource: "LICENSE_3RD_PARTY", withExtension: "txt")

//MARK: - Functions

func read(from url: URL) throws -> String {
	do {
		return try String(contentsOf: url, encoding: .utf8)
	} catch {
		throw DocumentsError.readError(url: url)
	}

}

func write(to url: URL, string: String, noEol: Bool = false) throws {
	var newString = string
	if !noEol {
		newString += "\n"
	}

	do {
		try newString.write(to: url, atomically: false, encoding: .utf8)
	} catch {
		throw DocumentsError.writeError(url: url)
	}

}

func append(to url: URL, string: String) {
	// Handle errors here instead of propagating them so that append errors do not interrupt other functions.
	do {
		var data = try read(from: url)
		data += string
		try write(to: url, string: data)

	} catch DocumentsError.readError(let url) {
		log("Error reading file \"\(url)\" while appending the following message: " + string)
	} catch DocumentsError.writeError(let url) {
		log("Error writing to file \"\(url)\" while appending the following message: " + string)
	} catch {
		log("Unknown error while appending the following message: " + string)
	}

}

func clearData(in url: URL) throws {
	do {
		try write(to: url, string: "", noEol: true)
	} catch {
		throw DocumentsError.writeErrorWhileClearing(url: url)
	}

}
