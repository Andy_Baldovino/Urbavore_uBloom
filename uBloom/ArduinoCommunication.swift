//
//  ArduinoCommunication.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-05-31.
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import Foundation

/// Possible errors when communicating with Arduino.
enum CommunicationError: Error {
	case invalidMessage
	case insufficientComponents
	case unexpectedMessage(state: CommunicationState)
	case unexpectedPar
	case invalidLevels
	case invalidWater
	case invalidTemp
	case invalidCond
	case invalidPh
	case invalidPar1
	case invalidPar2
	case invalidPar3
	case insufficientPar
	case validationFailed(expected: String, received: String)
}

/// Possible message types.
private enum MessageType: String {
	// Receiving types.
	case receiveLevels = "C"
	case receiveTimestamp = "T"
	case receiveSump1 = "Q"
	case receiveSump2 = "R"
	case receivePar = "S"

	// Sending types.
	case sendSump1 = "K"
	case sendSump2 = "L"
	case sendPar = "M"
	case sendMist = "N"

	// Both.
	case ok = "O"
}

/// Possible states when communicating with Arduino.
enum CommunicationState: String {
	case idle = "Idle"

	case timestampReceived = "Timestamp received"
	case sump1Received = "First sump data received"
	case sump2Received = "Second sump data received"
	case parReceived = "PAR data received"

	case sump1Sent = "First sump thresholds sent"
	case sump2Sent = "Second sump thresholds sent"
	case parSent = "PAR thresholds sent"
	case mistSent = "Misting thresholds sent"
}

//MARK: - Constants
let delimiter = ","
let maxLevels = 3

//MARK: - Variables

// The most recent sensor values received.
var water = Sensor(description: "Water Level", threshold: Defaults.water, deviation: 10)
var temp = Sensor(description: "Temperature", threshold: Defaults.temp, deviation: 10)
var cond = Sensor(description: "Conductivity", threshold: Defaults.cond, deviation: 10)
var ph = Sensor(description: "pH", threshold: Defaults.ph, deviation: 1)
var par1 = Sensor(description: "Level 1 PAR", threshold: Defaults.par1, deviation: 10)
var par2 = Sensor(description: "Level 2 PAR", threshold: Defaults.par2, deviation: 10)
var par3 = Sensor(description: "Level 3 PAR", threshold: Defaults.par3, deviation: 10)

var levels = 0
private var levelsToSet: Int?
private var record = ""
private var state: CommunicationState = .idle
private var waitingToSend = false
private var check = ""

//MARK: - Functions

private func prepareRecord(using components: [String]) {
	for i in 1..<components.count {
		record += delimiter
		record += components[i]
	}

}

private func receivedSump1(in components: [String]) throws {
	guard components.count >= 3 else {
		throw CommunicationError.insufficientComponents
	}
	guard let waterValue = Double(components[1]) else {
		throw CommunicationError.invalidWater
	}
	guard let tempValue = Double(components[2]) else {
		throw CommunicationError.invalidTemp
	}

	water.value = waterValue
	temp.value = tempValue
	prepareRecord(using: components)
}

private func receivedSump2(in components: [String]) throws {
	guard components.count >= 3 else {
		throw CommunicationError.insufficientComponents
	}
	guard let condValue = Double(components[1]) else {
		throw CommunicationError.invalidCond
	}
	guard let phValue = Double(components[2]) else {
		throw CommunicationError.invalidPh
	}

	cond.value = condValue
	ph.value = phValue
	prepareRecord(using: components)
}

private func processPar1(in component: String) throws {
	guard let value = Double(component) else {
		throw CommunicationError.invalidPar1
	}

	par1.value = value
}

private func processPar2(in component: String) throws {
	guard let value = Double(component) else {
		throw CommunicationError.invalidPar2
	}

	par2.value = value
}

private func processPar3(in component: String) throws {
	guard let value = Double(component) else {
		throw CommunicationError.invalidPar3
	}

	par3.value = value
}

private func receivedPar(in components: [String]) throws {
	guard components.count >= (levels + 1) else {
		throw CommunicationError.insufficientPar
	}

	var parComponents = components
	while parComponents.count > (levels + 1) {
		parComponents.removeLast() // Remove extra PAR components.
	}

	switch levels {
	case 1:
		try processPar1(in: parComponents[1])

	case 2:
		try processPar1(in: parComponents[1])
		try processPar2(in: parComponents[2])

	case 3:
		try processPar1(in: parComponents[1])
		try processPar2(in: parComponents[2])
		try processPar3(in: parComponents[3])

	default:
		break
	}

	prepareRecord(using: parComponents)
}

private func endReceive() {
	state = .idle

	if let newLevels = levelsToSet {
		levels = newLevels
		levelsToSet = nil
	}

	if waitingToSend {
		waitingToSend = false
		sendThresholds()
	}

}

private func sendPayload(type: MessageType, components: String...) -> String {
	var payload = type.rawValue
	for i in 0..<components.count {
		payload += delimiter
		payload += components[i]
	}

	serial.sendMessageToDevice(payload)

	return payload
}

private func endSend() {
	state = .idle
}

func parse(_ input: String) throws -> Bool {
	let components = input.components(separatedBy: delimiter)
	guard let type = MessageType(rawValue: components[0]) else {
		throw CommunicationError.invalidMessage
	}

	switch type {
	// Receiving types.
	case .receiveLevels:
		guard components.count >= 2 else {
			throw CommunicationError.insufficientComponents
		}
		guard let levelsReceived = Int(components[1]), levelsReceived >= 0, levelsReceived <= maxLevels else {
			throw CommunicationError.invalidLevels
		}

		if state == .idle {
			levels = levelsReceived
		} else {
			levelsToSet = levelsReceived
		}

	case .receiveTimestamp:
		guard state == .idle else {
			let oldState = state
			endReceive()
			throw CommunicationError.unexpectedMessage(state: oldState)
		}

		do {
			try record = setTimestamp(using: components) // Start a new record if timestamp is valid.
		} catch {
			endReceive()
			throw error
		}
		serial.sendMessageToDevice(input)
		state = .timestampReceived

	case .receiveSump1:
		guard state == .timestampReceived || state == .sump2Received || state == .parReceived else {
			let oldState = state
			endReceive()
			throw CommunicationError.unexpectedMessage(state: oldState)
		}

		if state != .timestampReceived {
			// Successfully received previous record.
			append(to: recordsUrl, string: record)

			record = addMinutesToLastTimestamp(Defaults.update) // Start a new record.
		}

		do {
			try receivedSump1(in: components)
		} catch {
			endReceive()
			throw error
		}

		serial.sendMessageToDevice(input)
		state = .sump1Received

	case .receiveSump2:
		guard state == .sump1Received else {
			let oldState = state
			endReceive()
			throw CommunicationError.unexpectedMessage(state: oldState)
		}

		do {
			try receivedSump2(in: components)
		} catch {
			endReceive()
			throw error
		}

		serial.sendMessageToDevice(input)
		state = .sump2Received

	case .receivePar:
		guard state == .sump2Received else {
			let oldState = state
			endReceive()
			throw CommunicationError.unexpectedMessage(state: oldState)
		}
		guard levels != 0 else {
			endReceive()
			throw CommunicationError.unexpectedPar
		}

		do {
			try receivedPar(in: components)
		} catch {
			endReceive()
			throw error
		}

		serial.sendMessageToDevice(input)
		state = .parReceived

	// Sending types.
	case .sendSump1:
		guard input == check else {
			endSend()
			throw CommunicationError.validationFailed(expected: check, received: input)
		}

		check = sendPayload(type: .sendSump2, components: Defaults.condString!, Defaults.phString!)
		state = .sump2Sent

	case .sendSump2:
		guard input == check else {
			endSend()
			throw CommunicationError.validationFailed(expected: check, received: input)
		}

		check = sendPayload(type: .sendPar, components: Defaults.par1String!, Defaults.par2String!, Defaults.par3String!)
		state = .parSent

	case .sendPar:
		guard input == check else {
			endSend()
			throw CommunicationError.validationFailed(expected: check, received: input)
		}

		check = sendPayload(type: .sendMist, components: Defaults.mist1String!, Defaults.mist2String!, Defaults.mist3String!)
		state = .mistSent

	case .sendMist:
		guard input == check else {
			endSend()
			throw CommunicationError.validationFailed(expected: check, received: input)
		}

		serial.sendMessageToDevice("O")
		endSend()

	// Both.
	case .ok:
		guard state == .sump2Received || state == .parReceived else {
			let oldState = state
			endReceive()
			throw CommunicationError.unexpectedMessage(state: oldState)
		}

		// Successfully received final record.
		append(to: recordsUrl, string: record)
		endReceive()
	}

	if state == .idle {
		return true
	} else {
		return false
	}

}

func sendThresholds() {
	guard state == .idle else {
		waitingToSend = true
		return
	}

	check = sendPayload(type: .sendSump1, components: Defaults.waterString!, Defaults.tempString!, Defaults.updateString!)
	state = .sump1Sent
}
