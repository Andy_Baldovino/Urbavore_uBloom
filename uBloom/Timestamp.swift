//
//  Timestamp.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-05-05.
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import Foundation

/// Possible errors when handling timestamps.
enum TimestampError: Error {
	case insufficientComponents
	case invalidYear
	case invalidMonth
	case invalidDay
	case invalidHour
	case invalidMinute
}

func getNumberOfDays(month: Int, year: Int) -> Int {
	switch month {
	case 2:
		if year % 4 == 0 {
			// Always a leap year in years 2000 to 2099.
			return 29
		} else {
			// Common year.
			return 28
		}

	case 4, 6, 9, 11:
		return 30

	default:
		return 31
	}
}

func prepareTimestampString() -> String {
	var timestampString = Defaults.yearString!
	timestampString += delimiter
	timestampString += Defaults.monthString!
	timestampString += delimiter
	timestampString += Defaults.dayString!
	timestampString += delimiter
	timestampString += Defaults.hourString!
	timestampString += delimiter
	timestampString += Defaults.minuteString!

	return timestampString
}

func setTimestamp(using component: [String]) throws -> String {

	guard component.count >= 6 else {
		throw TimestampError.insufficientComponents
	}

	guard let year = Int(component[1]), year >= 0, year <= 99 else {
		throw TimestampError.invalidYear
	}

	guard let month = Int(component[2]), month >= 1, month <= 12 else {
		throw TimestampError.invalidMonth
	}

	guard let day = Int(component[3]), day >= 1, day <= getNumberOfDays(month: month, year: year) else {
		throw TimestampError.invalidDay
	}

	guard let hour = Int(component[4]), hour >= 0, hour <= 23 else {
		throw TimestampError.invalidHour
	}

	guard let minute = Int(component[5]), minute >= 0, minute <= 59 else {
		throw TimestampError.invalidMinute
	}

	Defaults.year = year
	Defaults.month = month
	Defaults.day = day
	Defaults.hour = hour
	Defaults.minute = minute

	return prepareTimestampString()
}

func addMinutesToLastTimestamp(_ minutes: Int) -> String {
	Defaults.minute += minutes

	while Defaults.minute >= 60 {
		Defaults.minute -= 60
		Defaults.hour += 1
	}

	while Defaults.hour >= 24 {
		Defaults.hour -= 24
		Defaults.day += 1
	}

	while Defaults.day > getNumberOfDays(month: Defaults.month, year: Defaults.year) {
		Defaults.day -= getNumberOfDays(month: Defaults.month, year: Defaults.year)
		Defaults.month += 1
	}

	while Defaults.month > 12 {
		Defaults.month -= 12
		Defaults.year += 1
	}

	Defaults.year %= 100 // Loop year back to 0 after 99.

	return prepareTimestampString()
}

func getDouble(year: Int, month: Int, day: Int) -> Double {
	let daysInMonth = Double(getNumberOfDays(month: month, year: year))

	var time = Double(year) + 2000
	time += ( Double(month) - 1 ) / 12
	time += ( Double(day) - 1 ) / 12 / daysInMonth

	return time
}

func getDouble(year: Int, month: Int, day: Int, hour: Int) -> Double {
	let daysInMonth = Double(getNumberOfDays(month: month, year: year))

	var time = Double(year) + 2000
	time += ( Double(month) - 1 ) / 12
	time += ( Double(day) - 1 ) / 12 / daysInMonth
	time += Double(hour) / 12 / daysInMonth / 24

	return time
}

func getDouble(year: Int, month: Int, day: Int, hour: Int, minute: Int) -> Double {
	let daysInMonth = Double(getNumberOfDays(month: month, year: year))

	var time = Double(year) + 2000
	time += ( Double(month) - 1 ) / 12
	time += ( Double(day) - 1 ) / 12 / daysInMonth
	time += Double(hour) / 12 / daysInMonth / 24
	time += Double(minute) / 12 / daysInMonth / 24 / 60

	return time
}
