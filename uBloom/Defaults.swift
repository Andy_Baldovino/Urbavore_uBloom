//
//  Defaults.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-05-05.
//  Adapted from http://www.thomashanning.com/userdefaults/
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import Foundation

class Defaults {

	/// Possible key names for UserDefaults.
	private struct Key {
		static let water = "water"
		static let temp = "temp"
		static let cond = "cond"
		static let ph = "ph"
		static let par1 = "par1"
		static let par2 = "par2"
		static let par3 = "par3"
		static let mist1 = "mist1"
		static let mist2 = "mist2"
		static let mist3 = "mist3"
		static let update = "update"
		static let year = "year"
		static let month = "month"
		static let day = "day"
		static let hour = "hour"
		static let minute = "minute"
		static let recordsFileCreated = "recordsFileCreated"
		static let errorsFileCreated = "errorsFileCreated"
	}

	//MARK: - Variables

	static var water: Double {
		get {
			return UserDefaults.standard.double(forKey: Key.water)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: Key.water)
		}
	}
	static var waterString: String? {
		return UserDefaults.standard.string(forKey: Key.water)
	}

	static var temp: Double {
		get {
			return UserDefaults.standard.double(forKey: Key.temp)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: Key.temp)
		}
	}
	static var tempString: String? {
		return UserDefaults.standard.string(forKey: Key.temp)
	}

	static var cond: Double {
		get {
			return UserDefaults.standard.double(forKey: Key.cond)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: Key.cond)
		}
	}
	static var condString: String? {
		return UserDefaults.standard.string(forKey: Key.cond)
	}

	static var ph: Double {
		get {
			return UserDefaults.standard.double(forKey: Key.ph)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: Key.ph)
		}
	}
	static var phString: String? {
		return UserDefaults.standard.string(forKey: Key.ph)
	}

	static var par1: Double {
		get {
			return UserDefaults.standard.double(forKey: Key.par1)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: Key.par1)
		}
	}
	static var par1String: String? {
		return UserDefaults.standard.string(forKey: Key.par1)
	}

	static var par2: Double {
		get {
			return UserDefaults.standard.double(forKey: Key.par2)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: Key.par2)
		}
	}
	static var par2String: String? {
		return UserDefaults.standard.string(forKey: Key.par2)
	}

	static var par3: Double {
		get {
			return UserDefaults.standard.double(forKey: Key.par3)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: Key.par3)
		}
	}
	static var par3String: String? {
		return UserDefaults.standard.string(forKey: Key.par3)
	}

	static var mist1: Int {
		get {
			return UserDefaults.standard.integer(forKey: Key.mist1)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: Key.mist1)
		}
	}
	static var mist1String: String? {
		return UserDefaults.standard.string(forKey: Key.mist1)
	}

	static var mist2: Int {
		get {
			return UserDefaults.standard.integer(forKey: Key.mist2)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: Key.mist2)
		}
	}
	static var mist2String: String? {
		return UserDefaults.standard.string(forKey: Key.mist2)
	}

	static var mist3: Int {
		get {
			return UserDefaults.standard.integer(forKey: Key.mist3)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: Key.mist3)
		}
	}
	static var mist3String: String? {
		return UserDefaults.standard.string(forKey: Key.mist3)
	}

	static var update: Int {
		get {
			return UserDefaults.standard.integer(forKey: Key.update)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: Key.update)
		}
	}
	static var updateString: String? {
		return UserDefaults.standard.string(forKey: Key.update)
	}

	static var year: Int {
		get {
			return UserDefaults.standard.integer(forKey: Key.year)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: Key.year)
		}
	}
	static var yearString: String? {
		return UserDefaults.standard.string(forKey: Key.year)
	}

	static var month: Int {
		get {
			return UserDefaults.standard.integer(forKey: Key.month)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: Key.month)
		}
	}
	static var monthString: String? {
		return UserDefaults.standard.string(forKey: Key.month)
	}

	static var day: Int {
		get {
			return UserDefaults.standard.integer(forKey: Key.day)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: Key.day)
		}
	}
	static var dayString: String? {
		return UserDefaults.standard.string(forKey: Key.day)
	}

	static var hour: Int {
		get {
			return UserDefaults.standard.integer(forKey: Key.hour)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: Key.hour)
		}
	}
	static var hourString: String? {
		return UserDefaults.standard.string(forKey: Key.hour)
	}

	static var minute: Int {
		get {
			return UserDefaults.standard.integer(forKey: Key.minute)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: Key.minute)
		}
	}
	static var minuteString: String? {
		return UserDefaults.standard.string(forKey: Key.minute)
	}

	static var recordsFileCreated: Bool {
		get {
			return UserDefaults.standard.bool(forKey: Key.recordsFileCreated)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: Key.recordsFileCreated)
		}
	}

	static var errorsFileCreated: Bool {
		get {
			return UserDefaults.standard.bool(forKey: Key.errorsFileCreated)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: Key.errorsFileCreated)
		}
	}

}
