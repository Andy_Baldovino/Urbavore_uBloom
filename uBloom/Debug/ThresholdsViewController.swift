//
//  ThresholdsViewController.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-03-14.
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import UIKit
import CoreBluetooth

class ThresholdsViewController: UIViewController, UITextFieldDelegate, BluetoothSerialDelegate {

	//MARK: IBOutlets

	@IBOutlet weak var waterLevelThreshold: UITextField!
	@IBOutlet weak var sumpTemperatureThreshold: UITextField!
	@IBOutlet weak var sumpConductivityThreshold: UITextField!
	@IBOutlet weak var sumpPhThreshold: UITextField!
	@IBOutlet weak var level1ParThreshold: UITextField!
	@IBOutlet weak var level2ParThreshold: UITextField!
	@IBOutlet weak var level3ParThreshold: UITextField!
	@IBOutlet weak var level1MistingDuration: UITextField!
	@IBOutlet weak var level2MistingDuration: UITextField!
	@IBOutlet weak var level3MistingDuration: UITextField!
	@IBOutlet weak var sensorUpdateInterval: UITextField!
	@IBOutlet weak var applyButton: UIButton!

	//MARK: - Functions

	override func viewDidLoad() {
		super.viewDidLoad()

		// Load user settings.
		waterLevelThreshold.text = Defaults.waterString
		sumpTemperatureThreshold.text = Defaults.tempString
		sumpConductivityThreshold.text = Defaults.condString
		sumpPhThreshold.text = Defaults.phString
		level1ParThreshold.text = Defaults.par1String
		level2ParThreshold.text = Defaults.par2String
		level3ParThreshold.text = Defaults.par3String
		level1MistingDuration.text = Defaults.mist1String
		level2MistingDuration.text = Defaults.mist2String
		level3MistingDuration.text = Defaults.mist3String
		sensorUpdateInterval.text = Defaults.updateString

		waterLevelThreshold.delegate = self
		sumpTemperatureThreshold.delegate = self
		sumpConductivityThreshold.delegate = self
		sumpPhThreshold.delegate = self
		level1ParThreshold.delegate = self
		level2ParThreshold.delegate = self
		level3ParThreshold.delegate = self
		level1MistingDuration.delegate = self
		level2MistingDuration.delegate = self
		level3MistingDuration.delegate = self
		sensorUpdateInterval.delegate = self
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		serial.delegate = self
		refreshApplyButton()
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()

		// Dispose of any resources that can be recreated.
	}

	func refreshApplyButton() {
		if serial.isReady {
			applyButton.isEnabled = true
			applyButton.setTitle("Apply and Send to \(serial.connectedPeripheral!.name ?? "Device")", for: .normal)
		} else if serial.isPoweredOn {
			applyButton.isEnabled = false
			applyButton.setTitle("No Device Connected", for: .normal)
		} else {
			applyButton.isEnabled = false
			applyButton.setTitle("Bluetooth Disabled", for: .normal)
		}
	}

	//MARK: - UITextFieldDelegate

	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		// Hide the keyboard.
		textField.resignFirstResponder()

		return true
	}

	//MARK: - BluetoothSerialDelegate

	func serialDidChangeState() {
		if !serial.isPoweredOn {
			// If Bluetooth was turned off.
		}
		refreshApplyButton()
	}

	func serialDidDisconnect(_ peripheral: CBPeripheral, error: NSError?) {
		// If peripheral disconnected.
		refreshApplyButton()
	}

	func serialDidReceiveString(_ message: String) {
		do {
			if try parse(message) {
				// If communication is finished.
				refreshApplyButton()
			}
		} catch {
			logError(error, with: message)
		}

	}

	//MARK: - IBActions

	@IBAction func applyPressed(_ sender: UIButton) {
		// Do nothing if any field is empty or not a number.
		guard let waterField = waterLevelThreshold.text, let water = Double(waterField) else {
			return
		}
		guard let tempField = sumpTemperatureThreshold.text, let temp = Double(tempField) else {
			return
		}
		guard let condField = sumpConductivityThreshold.text, let cond = Double(condField) else {
			return
		}
		guard let phField = sumpPhThreshold.text, let ph = Double(phField) else {
			return
		}
		guard let par1Field = level1ParThreshold.text, let par1 = Double(par1Field) else {
			return
		}
		guard let par2Field = level2ParThreshold.text, let par2 = Double(par2Field) else {
			return
		}
		guard let par3Field = level3ParThreshold.text, let par3 = Double(par3Field) else {
			return
		}
		guard let mist1Field = level1MistingDuration.text, let mist1 = Int(mist1Field) else {
			return
		}
		guard let mist2Field = level2MistingDuration.text, let mist2 = Int(mist2Field) else {
			return
		}
		guard let mist3Field = level3MistingDuration.text, let mist3 = Int(mist3Field) else {
			return
		}
		guard let updateField = sensorUpdateInterval.text, let update = Int(updateField) else {
			return
		}

		applyButton.isEnabled = false
		applyButton.setTitle("Sending to \(serial.connectedPeripheral!.name ?? "Device")…", for: .normal)

		Defaults.water = water
		Defaults.temp = temp
		Defaults.cond = cond
		Defaults.ph = ph
		Defaults.par1 = par1
		Defaults.par2 = par2
		Defaults.par3 = par3
		Defaults.mist1 = mist1
		Defaults.mist2 = mist2
		Defaults.mist3 = mist3
		Defaults.update = update

		sendThresholds()
	}

	@IBAction func resetPressed(_ sender: UIButton) {
		Defaults.water = 500
		Defaults.temp = 500
		Defaults.cond = 500
		Defaults.ph = 7
		Defaults.par1 = 500
		Defaults.par2 = 500
		Defaults.par3 = 500
		Defaults.mist1 = 600
		Defaults.mist2 = 600
		Defaults.mist3 = 600
		Defaults.update = 10

		waterLevelThreshold.text = Defaults.waterString
		sumpTemperatureThreshold.text = Defaults.tempString
		sumpConductivityThreshold.text = Defaults.condString
		sumpPhThreshold.text = Defaults.phString
		level1ParThreshold.text = Defaults.par1String
		level2ParThreshold.text = Defaults.par2String
		level3ParThreshold.text = Defaults.par3String
		level1MistingDuration.text = Defaults.mist1String
		level2MistingDuration.text = Defaults.mist2String
		level3MistingDuration.text = Defaults.mist3String
		sensorUpdateInterval.text = Defaults.updateString
	}

}
