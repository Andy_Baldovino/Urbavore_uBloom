//
//  SensorsViewController.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-03-12.
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import UIKit
import CoreBluetooth

class SensorsViewController: UIViewController, BluetoothSerialDelegate {

	//MARK: - IBOutlets

	@IBOutlet weak var waterLevelValue: UILabel!
	@IBOutlet weak var waterLevelStatus: UILabel!
	@IBOutlet weak var sumpTemperatureValue: UILabel!
	@IBOutlet weak var sumpTemperatureStatus: UILabel!
	@IBOutlet weak var sumpConductivityValue: UILabel!
	@IBOutlet weak var sumpConductivityStatus: UILabel!
	@IBOutlet weak var sumpPhValue: UILabel!
	@IBOutlet weak var sumpPhStatus: UILabel!
	@IBOutlet weak var level1ParValue: UILabel!
	@IBOutlet weak var level1ParStatus: UILabel!
	@IBOutlet weak var level2ParValue: UILabel!
	@IBOutlet weak var level2ParStatus: UILabel!
	@IBOutlet weak var level3ParValue: UILabel!
	@IBOutlet weak var level3ParStatus: UILabel!

	//MARK: - Functions

	override func viewDidLoad() {
		super.viewDidLoad()

		performInitialSetup()

		// Initialize serial.
		serial = BluetoothSerial(delegate: self)

		water.attachLabels(value: waterLevelValue, status: waterLevelStatus)
		temp.attachLabels(value: sumpTemperatureValue, status: sumpTemperatureStatus)
		cond.attachLabels(value: sumpConductivityValue, status: sumpConductivityStatus)
		ph.attachLabels(value: sumpPhValue, status: sumpPhStatus)
		par1.attachLabels(value: level1ParValue, status: level1ParStatus)
		par2.attachLabels(value: level2ParValue, status: level2ParStatus)
		par3.attachLabels(value: level3ParValue, status: level3ParStatus)
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		serial.delegate = self

		// Load user thresholds, in case user saved new ones.
		water.threshold = Defaults.water
		temp.threshold = Defaults.temp
		cond.threshold = Defaults.cond
		ph.threshold = Defaults.ph
		par1.threshold = Defaults.par1
		par2.threshold = Defaults.par2
		par3.threshold = Defaults.par3
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()

		// Dispose of any resources that can be recreated.
	}

	//MARK: - BluetoothSerialDelegate

	func serialDidChangeState() {
		if !serial.isPoweredOn {
			// If Bluetooth was turned off.
		}
	}

	func serialDidDisconnect(_ peripheral: CBPeripheral, error: NSError?) {
		// If peripheral disconnected.
	}

	func serialDidReceiveString(_ message: String) {
		do {
			let _ = try parse(message)
		} catch {
			logError(error, with: message)
		}

	}

	//MARK: - IBActions

	@IBAction func clearRecordsPressed(_ sender: UIButton) {
		do {
			try clearData(in: recordsUrl)
		} catch {
			logError(error)
		}

	}

	@IBAction func clearErrorsPressed(_ sender: UIButton) {
		do {
			try clearData(in: errorsUrl)
		} catch {
			logError(error)
		}

	}

}
