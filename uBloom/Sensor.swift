//
//  Sensor.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-05-23.
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import UIKit

/// Possible sensor statuses.
enum SensorStatus: String {
	case low = "Low"
	case ok = "OK"
	case high = "High"
}

class Sensor {

	//MARK: Variables

	var description: String
	var valueLabel: UILabel?
	var statusLabel: UILabel?
	var unifiedLabel: UILabel?
	var image: UIImageView?

	var value: Double? {
		didSet {
			if let valLabel = valueLabel {
				if value == nil {
					valLabel.text = ""
				} else {
					valLabel.text = String(value!)
				}

			}

			updateStatusLabel()
			updateUnifiedLabel()
		}

	}

	var threshold: Double {
		didSet {
			updateStatusLabel()
			updateUnifiedLabel()
		}

	}

	var deviation: Double {
		didSet {
			updateStatusLabel()
			updateUnifiedLabel()
		}

	}

	var status: SensorStatus? {
		guard let reading = value else {
			return nil
		}

		if reading > (threshold + deviation) {
			return .high
		} else if reading < (threshold - deviation) {
			return .low
		} else {
			return .ok
		}

	}

	//MARK: - Functions

	init(description: String, threshold: Double, deviation: Double) {
		self.description = description
		self.threshold = threshold
		self.deviation = deviation
	}

	func attachLabels(value: UILabel? = nil, status: UILabel? = nil, unified: UILabel? = nil, image: UIImageView? = nil) {
		valueLabel = value
		statusLabel = status
		unifiedLabel = unified
		self.image = image
		updateStatusLabel()
		updateUnifiedLabel()
	}

	private func updateStatusLabel() {
		guard let label = statusLabel else {
			return
		}
		guard value != nil else {
			label.text = ""
			return
		}

		switch status! {
		case .ok:
			label.textColor = UIColor.green
		default:
			label.textColor = UIColor.red
		}

		label.text = status!.rawValue
	}

	private func updateUnifiedLabel() {
		guard let label = unifiedLabel else {
			return
		}
		guard let val = value else {
			label.text = "No data"
			return
		}
		guard let img = image else {
			return
		}

		label.text = "\(status!.rawValue): \(val)"
		img.image = UIImage(named: "\(description).\(status!.rawValue).png")
	}

	func updateSensorDetailsView(image: UIImageView, status: UILabel, value: UILabel, thresholds: UILabel) {
		var statusText = description + " is "
		if let sensorStatus = self.status {
			statusText += sensorStatus.rawValue
			image.image = UIImage(named: "\(description).\(sensorStatus.rawValue).png")
		} else {
			statusText += "unknown"
			image.image = UIImage(named: "\(description).Low.png")
		}
		status.text = statusText + "."

		var valueText = "Current value: "
		if let sensorValue = self.value {
			valueText += String(sensorValue)
		} else {
			valueText += "No data"
		}
		value.text = valueText

		thresholds.text = "Acceptable values: \(self.threshold - deviation)–\(self.threshold + deviation)"
	}

}
