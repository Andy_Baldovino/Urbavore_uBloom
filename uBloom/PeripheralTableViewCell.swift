//
//  PeripheralTableViewCell.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-03-12.
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import UIKit

class PeripheralTableViewCell: UITableViewCell {

	//MARK: IBOutlets

	@IBOutlet weak var peripheralName: UILabel!

	//MARK: - Functions

	override func awakeFromNib() {
		super.awakeFromNib()

		// Initialization code.
	}

	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)

		// Configure the view for the selected state.
	}

}
