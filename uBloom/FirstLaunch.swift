//
//  FirstLaunch.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-06-03.
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import Foundation

func performInitialSetup() {

	// Set default user settings if not already set.
	if Defaults.waterString == nil {
		Defaults.water = 500
	}
	if Defaults.tempString == nil {
		Defaults.temp = 500
	}
	if Defaults.condString == nil {
		Defaults.cond = 500
	}
	if Defaults.phString == nil {
		Defaults.ph = 7
	}
	if Defaults.par1String == nil {
		Defaults.par1 = 500
	}
	if Defaults.par2String == nil {
		Defaults.par2 = 500
	}
	if Defaults.par3String == nil {
		Defaults.par3 = 500
	}
	if Defaults.mist1String == nil {
		Defaults.mist1 = 600
	}
	if Defaults.mist2String == nil {
		Defaults.mist2 = 600
	}
	if Defaults.mist3String == nil {
		Defaults.mist3 = 600
	}
	if Defaults.updateString == nil {
		Defaults.update = 10
	}

	// Create files on device to store data if they do not exist yet.
	if !Defaults.recordsFileCreated {
		do {
			try clearData(in: recordsUrl)
		} catch {
			logError(error)
		}
		Defaults.recordsFileCreated = true
	}
	if !Defaults.errorsFileCreated {
		do {
			try clearData(in: errorsUrl)
		} catch {
			logError(error)
		}
		Defaults.errorsFileCreated = true
	}

}
