//
//  Error.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-05-09.
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import Foundation

func log(_ error: String) {
	let currentDate = Date()
	let dateFormatter = DateFormatter()
	dateFormatter.dateFormat = "[yyyy-MM-dd HH:mm:ss] "
	let dateString = dateFormatter.string(from: currentDate)

	// Ignore errors encountered while attempting to log errors.
	do {
		var data = try read(from: errorsUrl)
		data += dateString + error
		try write(to: errorsUrl, string: data)

	} catch {}
}

func logError(_ error: Error, with message: String) {
	switch error {
	case CommunicationError.invalidMessage:
		log("Received invalid message: " + message)
	case CommunicationError.insufficientComponents:
		log("Did not receive enough components: " + message)
	case CommunicationError.unexpectedMessage(let state):
		log("Did not expect the following message in state \"\(state.rawValue)\": " + message)
	case CommunicationError.unexpectedPar:
		log("Received PAR data but did not expect any: " + message)
	case CommunicationError.invalidLevels:
		log("Levels received is not a number or is out of range: " + message)
	case CommunicationError.invalidWater:
		log("Water Level received is not a number: " + message)
	case CommunicationError.invalidTemp:
		log("Temperature received is not a number: " + message)
	case CommunicationError.invalidCond:
		log("Conductivity received is not a number: " + message)
	case CommunicationError.invalidPh:
		log("pH received is not a number: " + message)
	case CommunicationError.invalidPar1:
		log("Level 1 PAR received is not a number: " + message)
	case CommunicationError.invalidPar2:
		log("Level 2 PAR received is not a number: " + message)
	case CommunicationError.invalidPar3:
		log("Level 3 PAR received is not a number: " + message)
	case CommunicationError.insufficientPar:
		log("Did not receive enough PAR values: " + message)
	case CommunicationError.validationFailed(let expected, let received):
		log("Expected \"" + expected + "\" from Arduino but received: " + received)
	case TimestampError.insufficientComponents:
		log("Timestamp received is incomplete: " + message)
	case TimestampError.invalidYear:
		log("Invalid year received: " + message)
	case TimestampError.invalidMonth:
		log("Invalid month received: " + message)
	case TimestampError.invalidDay:
		log("Invalid day received: " + message)
	case TimestampError.invalidHour:
		log("Invalid hour received: " + message)
	case TimestampError.invalidMinute:
		log("Invalid minute received: " + message)
	case DocumentsError.writeError(let url):
		log("Error writing the following message to file \"\(url)\": " + message)
	default:
		log("Unknown error: " + message)
	}

}

func logError(_ error: Error) {
	switch error {
	case DocumentsError.readError(let url):
		log("Error reading file \"\(url)\"")
	case DocumentsError.writeErrorWhileClearing(let url):
		log("Error clearing file \"\(url)\"")
	default:
		log("Unknown error")
	}

}
