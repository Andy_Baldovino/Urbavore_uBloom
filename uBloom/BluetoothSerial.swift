//
//  BluetoothSerial.swift
//  HM10 Serial
//
//  Created by Alex on 2015-08-09.
//  Copyright (c) 2017 Hangar42. All rights reserved.
//

import Foundation
import CoreBluetooth

// BLE peripheral to use: true for Bluno, false for HM10.
private let bluno = true

/// Global serial handler; don't forget to initialize it with init(delegate:).
var serial: BluetoothSerial!

protocol BluetoothSerialDelegate {
	// ** Required **

	/// Called when the state of the CBCentralManager changes (e.g., when bluetooth is turned on/off).
	func serialDidChangeState()

	/// Called when a peripheral disconnected.
	func serialDidDisconnect(_ peripheral: CBPeripheral, error: NSError?)

	// ** Optionals **

	/// Called when a string is received.
	func serialDidReceiveString(_ message: String)

	/// Called when an array of bytes is received.
	// func serialDidReceiveBytes(_ bytes: [UInt8])

	/// Called when data is received.
	// func serialDidReceiveData(_ data: Data)

	/// Called when the RSSI of the connected peripheral is read.
	func serialDidReadRSSI(_ rssi: NSNumber)

	/// Called when a new peripheral is discovered while scanning. Also gives the RSSI.
	func serialDidDiscoverPeripheral(_ peripheral: CBPeripheral, RSSI: NSNumber?)

	/// Called when a peripheral is connected (but not yet ready for communication).
	func serialDidConnect(_ peripheral: CBPeripheral)

	/// Called when a pending connection failed.
	func serialDidFailToConnect(_ peripheral: CBPeripheral, error: NSError?)

	/// Called when a peripheral is ready for communication.
	func serialIsReady(_ peripheral: CBPeripheral)
}

// Make some of the delegate functions optional.
extension BluetoothSerialDelegate {
	func serialDidReceiveString(_ message: String) {}
	// func serialDidReceiveBytes(_ bytes: [UInt8]) {}
	// func serialDidReceiveData(_ data: Data) {}
	func serialDidReadRSSI(_ rssi: NSNumber) {}
	func serialDidDiscoverPeripheral(_ peripheral: CBPeripheral, RSSI: NSNumber?) {}
	func serialDidConnect(_ peripheral: CBPeripheral) {}
	func serialDidFailToConnect(_ peripheral: CBPeripheral, error: NSError?) {}
	func serialIsReady(_ peripheral: CBPeripheral) {}
}

final class BluetoothSerial: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {

	//MARK: Variables

	/// The delegate object the BluetoothDelegate methods will be called upon.
	var delegate: BluetoothSerialDelegate!

	/// The CBCentralManager this Bluetooth serial handler uses.
	var centralManager: CBCentralManager!

	/// The peripheral we're trying to connect to (nil if none).
	var pendingPeripheral: CBPeripheral?

	/// The connected peripheral (nil if not connected).
	var connectedPeripheral: CBPeripheral?

	/// The characteristic 0xDFB1/0xFFE1 of the connectedPeripheral we need to write to.
	weak var writeCharacteristic: CBCharacteristic?

	/// Whether this serial is ready to send and receive data.
	var isReady: Bool {
		get {
			return centralManager.state == .poweredOn &&
				connectedPeripheral != nil &&
				writeCharacteristic != nil
		}

	}

	/// Whether this serial is looking for advertising peripherals.
	var isScanning: Bool {
		return centralManager.isScanning
	}

	/// Whether the state of the centralManager is .poweredOn.
	var isPoweredOn: Bool {
		return centralManager.state == .poweredOn
	}

	/// UUID of the service to look for.
	var serviceUUID = bluno ? CBUUID(string: "DFB0") : CBUUID(string: "FFE0")

	/// UUID of the characteristic to look for.
	var characteristicUUID = bluno ? CBUUID(string: "DFB1") : CBUUID(string: "FFE1")

	/// Whether to write to the HM10 with or without response. Set automatically.
	/// Legit HM10 modules (from JNHuaMao) require 'Write without Response',
	/// while fake modules (e.g. from Bolutek) require 'Write with Response'.
	private var writeType: CBCharacteristicWriteType = .withoutResponse

	//MARK: - Functions

	/// Always use this to initialize an instance.
	init(delegate: BluetoothSerialDelegate) {
		super.init()
		self.delegate = delegate
		centralManager = CBCentralManager(delegate: self, queue: nil)
	}

	/// Start scanning for peripherals.
	func startScan() {
		guard centralManager.state == .poweredOn else {
			return
		}

		// Start scanning for peripherals with correct service UUID.
		centralManager.scanForPeripherals(withServices: [serviceUUID], options: nil)

		// Retrieve peripherals that are already connected.
		// https://stackoverflow.com/questions/13286487
		let peripherals = centralManager.retrieveConnectedPeripherals(withServices: [serviceUUID])
		for peripheral in peripherals {
			delegate.serialDidDiscoverPeripheral(peripheral, RSSI: nil)
		}

	}

	/// Stop scanning for peripherals.
	func stopScan() {
		centralManager.stopScan()
	}

	/// Try to connect to the given peripheral.
	func connectToPeripheral(_ peripheral: CBPeripheral) {
		pendingPeripheral = peripheral
		centralManager.connect(peripheral, options: nil)
	}

	/// Disconnect from the connected peripheral or stop connecting to it.
	func disconnect() {
		if let p = connectedPeripheral {
			centralManager.cancelPeripheralConnection(p)
		} else if let p = pendingPeripheral {
			centralManager.cancelPeripheralConnection(p)
		}

	}

	/// The didReadRSSI delegate function will be called after calling this function.
	func readRSSI() {
		guard isReady else {
			return
		}

		connectedPeripheral!.readRSSI()
	}

	/// Send a string to the device.
	func sendMessageToDevice(_ message: String) {
		guard isReady else {
			return
		}

		if let data = message.data(using: String.Encoding.utf8) {
			connectedPeripheral!.writeValue(data, for: writeCharacteristic!, type: writeType)
		}

	}

	/// Send an array of bytes to the device.
	func sendBytesToDevice(_ bytes: [UInt8]) {
		guard isReady else {
			return
		}

		let data = Data(bytes: UnsafePointer<UInt8>(bytes), count: bytes.count)
		connectedPeripheral!.writeValue(data, for: writeCharacteristic!, type: writeType)
	}

	/// Send data to the device.
	func sendDataToDevice(_ data: Data) {
		guard isReady else {
			return
		}

		connectedPeripheral!.writeValue(data, for: writeCharacteristic!, type: writeType)
	}

	//MARK: - CBCentralManagerDelegate

	func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
		// Just send it to the delegate.
		delegate.serialDidDiscoverPeripheral(peripheral, RSSI: RSSI)
	}

	func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
		// Set some stuff right.
		peripheral.delegate = self
		pendingPeripheral = nil
		connectedPeripheral = peripheral

		// Send it to the delegate.
		delegate.serialDidConnect(peripheral)

		// The peripheral is connected but not yet ready for communication.
		// First get the 0xDFB0/0xFFE0 service.
		// Then get the 0xDFB1/0xFFE1 characteristic of this service.
		// Subscribe to it, create a weak reference to it (for writing later on),
		// and find out the writeType by looking at characteristic.properties.
		// Now we're ready for communication.
		peripheral.discoverServices([serviceUUID])
	}

	func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
		connectedPeripheral = nil
		pendingPeripheral = nil

		// Send it to the delegate.
		delegate.serialDidDisconnect(peripheral, error: error as NSError?)
	}

	func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
		pendingPeripheral = nil

		// Just send it to the delegate.
		delegate.serialDidFailToConnect(peripheral, error: error as NSError?)
	}

	func centralManagerDidUpdateState(_ central: CBCentralManager) {
		// Note that "didDisconnectPeripheral" won't be called if Bluetooth is turned off while connected.
		connectedPeripheral = nil
		pendingPeripheral = nil

		// Send it to the delegate.
		delegate.serialDidChangeState()
	}

	//MARK: - CBPeripheralDelegate

	func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
		// Discover the 0xDFB1/0xFFE1 characteristic for all services (though there should only be one).
		for service in peripheral.services! {
			peripheral.discoverCharacteristics([characteristicUUID], for: service)
		}

	}

	func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
		// Check whether the characteristic we're looking for (0xDFB1/0xFFE1) is present.
		for characteristic in service.characteristics! {
			if characteristic.uuid == characteristicUUID {
				// Subscribe to this value (so we'll get notified when there is serial data for us).
				peripheral.setNotifyValue(true, for: characteristic)

				// Keep a reference to this characteristic so we can write to it.
				writeCharacteristic = characteristic

				// Find out writeType.
				writeType = characteristic.properties.contains(.write) ? .withResponse : .withoutResponse

				// Notify the delegate we're ready for communication.
				delegate.serialIsReady(peripheral)
			}

		}

	}

	func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
		// Notify the delegate in different ways.
		// If you don't use one of these, comment it out for efficiency.
		let data = characteristic.value
		guard data != nil else {
			return
		}

		// First the data.
		// delegate.serialDidReceiveData(data!)

		// Then the string.
		if let str = String(data: data!, encoding: String.Encoding.utf8) {
			delegate.serialDidReceiveString(str)
		} else {
			// Uncomment for debugging.
			// print("Received an invalid string!")
		}

		// Finally the bytes array.
		/*
		var bytes = [UInt8](repeating: 0, count: data!.count / MemoryLayout<UInt8>.size)
		(data! as NSData).getBytes(&bytes, length: data!.count)
		delegate.serialDidReceiveBytes(bytes)
		*/
	}

	func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
		delegate.serialDidReadRSSI(RSSI)
	}

}
