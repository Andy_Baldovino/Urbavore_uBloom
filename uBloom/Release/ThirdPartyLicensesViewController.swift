//
//  ThirdPartyLicensesViewController.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-06-22.
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import UIKit

class ThirdPartyLicensesViewController: UIViewController {

	//MARK: IBOutlets

	@IBOutlet weak var textView: UITextView!

	//MARK: - Functions

	override func viewDidLoad() {
		super.viewDidLoad()

		var text = "This software uses third-party components which are distributed under the following terms:\n\n\n\n"

		guard let existingUrl = licenseUrl else {
			text += "Error reading licenses."
			textView.text = text
			return
		}

		do {
			text += try read(from: existingUrl)
		} catch {
			text += "Error reading licenses."
		}

		textView.text = text
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()

		// Dispose of any resources that can be recreated.
	}

	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()

		// Scroll to top of text view.
		textView.setContentOffset(.zero, animated: false)
	}

}
