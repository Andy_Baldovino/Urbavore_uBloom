//
//  NewLevel.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-06-10.
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import Foundation

protocol NewLevelDelegate {
	func newLevelSelected(level: Int)
}
