//
//  ParViewController.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-06-10.
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import UIKit
import CoreBluetooth

class ParViewController: UIViewController, BluetoothSerialDelegate {

	//MARK: IBOutlets

	@IBOutlet weak var image: UIImageView!
	@IBOutlet weak var status: UILabel!
	@IBOutlet weak var value: UILabel!
	@IBOutlet weak var thresholds: UILabel!
	@IBOutlet weak var levelSelect: UISegmentedControl!
	
	//MARK: - Variables

	var selectedLevel = 0
	var delegate: NewLevelDelegate!

	//MARK: - Functions

	override func viewDidLoad() {
		super.viewDidLoad()

		// Do any additional setup after loading the view.
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		levelSelect.selectedSegmentIndex = selectedLevel
		updateDetails()
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		serial.delegate = self

		if let segue = segueSensor {
			switch segue {
			case .par1:
				segueSensor = nil
				selectedLevel = 0
				levelSelect.selectedSegmentIndex = selectedLevel
				delegate.newLevelSelected(level: selectedLevel)
				updateDetails()

			case .par2:
				segueSensor = nil
				selectedLevel = 1
				levelSelect.selectedSegmentIndex = selectedLevel
				delegate.newLevelSelected(level: selectedLevel)
				updateDetails()

			case .par3:
				segueSensor = nil
				selectedLevel = 2
				levelSelect.selectedSegmentIndex = selectedLevel
				delegate.newLevelSelected(level: selectedLevel)
				updateDetails()

			default:
				performSegue(withIdentifier: "unwindParToDetails", sender: self)
			}

		}

	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()

		// Dispose of any resources that can be recreated.
	}

	func updateDetails() {
		switch selectedLevel {
		case UISegmentedControlNoSegment:
			title = "PAR"
			status.text = "PAR is unknown."
			value.text = "Current value: No data"
			thresholds.text = "Please select a growth level."

		case 0:
			title = "Level 1 PAR"
			par1.updateSensorDetailsView(image: image, status: status, value: value, thresholds: thresholds)
			if selectedLevel + 1 > levels {
				status.text = "Level 1 PAR is unknown."
				value.text = "Level 1 is not connected."
			}

		case 1:
			title = "Level 2 PAR"
			par2.updateSensorDetailsView(image: image, status: status, value: value, thresholds: thresholds)
			if selectedLevel + 1 > levels {
				status.text = "Level 2 PAR is unknown."
				value.text = "Level 2 is not connected."
			}

		case 2:
			title = "Level 3 PAR"
			par3.updateSensorDetailsView(image: image, status: status, value: value, thresholds: thresholds)
			if selectedLevel + 1 > levels {
				status.text = "Level 3 PAR is unknown."
				value.text = "Level 3 is not connected."
			}

		default:
			break
		}

	}

	//MARK: - BluetoothSerialDelegate

	func serialDidChangeState() {
		if !serial.isPoweredOn {
			// If Bluetooth was turned off.
		}
	}

	func serialDidDisconnect(_ peripheral: CBPeripheral, error: NSError?) {
		// If peripheral disconnected.
	}

	func serialDidReceiveString(_ message: String) {
		do {
			let _ = try parse(message)
			delegate.newLevelSelected(level: selectedLevel)
			updateDetails()

		} catch {
			logError(error, with: message)
		}

	}

	//MARK: - IBactions
	
	@IBAction func newLevelSelected(_ sender: UISegmentedControl) {
		selectedLevel = levelSelect.selectedSegmentIndex
		delegate.newLevelSelected(level: selectedLevel)
		updateDetails()
	}

}
