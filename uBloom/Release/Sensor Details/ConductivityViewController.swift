//
//  ConductivityViewController.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-06-10.
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import UIKit
import CoreBluetooth

class ConductivityViewController: UIViewController, BluetoothSerialDelegate {

	//MARK: IBOutlets
	
	@IBOutlet weak var image: UIImageView!
	@IBOutlet weak var status: UILabel!
	@IBOutlet weak var value: UILabel!
	@IBOutlet weak var thresholds: UILabel!

	//MARK: - Functions

	override func viewDidLoad() {
		super.viewDidLoad()

		// Do any additional setup after loading the view.
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		cond.updateSensorDetailsView(image: image, status: status, value: value, thresholds: thresholds)
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		serial.delegate = self

		if let segue = segueSensor {
			if segue == .cond {
				segueSensor = nil
			} else {
				performSegue(withIdentifier: "unwindCondToDetails", sender: self)
			}
		}
		
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()

		// Dispose of any resources that can be recreated.
	}

	//MARK: - BluetoothSerialDelegate

	func serialDidChangeState() {
		if !serial.isPoweredOn {
			// If Bluetooth was turned off.
		}
	}

	func serialDidDisconnect(_ peripheral: CBPeripheral, error: NSError?) {
		// If peripheral disconnected.
	}

	func serialDidReceiveString(_ message: String) {
		do {
			let _ = try parse(message)
			cond.updateSensorDetailsView(image: image, status: status, value: value, thresholds: thresholds)

		} catch {
			logError(error, with: message)
		}

	}

}
