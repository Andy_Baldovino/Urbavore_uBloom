//
//  AboutViewController.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-06-23.
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import UIKit

class AboutViewController: UIViewController {

	//MARK: Functions

	override func viewDidLoad() {
		super.viewDidLoad()

		// Do any additional setup after loading the view.
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()

		// Dispose of any resources that can be recreated.
	}

	//MARK: - IBActions

	@IBAction func websiteButtonPressed(_ sender: UIButton) {
		if let url = URL(string: "https://growurbavore.com") {
			UIApplication.shared.openURL(url)
		}

	}

}
