//
//  DetailsViewController.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-05-30.
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import UIKit
import CoreBluetooth

/// Posible detailed sensor views to switch to.
enum PossibleSegue {
	case water
	case temp
	case ph
	case cond
	case par1
	case par2
	case par3
}

/// Detailed sensor view to switch to upon opening this view.
var segueSensor: PossibleSegue?

class DetailsViewController: UIViewController, BluetoothSerialDelegate, NewLevelDelegate {

	//MARK: IBOutlets
	
	@IBOutlet weak var waterImage: UIImageView!
	@IBOutlet weak var tempImage: UIImageView!
	@IBOutlet weak var phImage: UIImageView!
	@IBOutlet weak var condImage: UIImageView!
	@IBOutlet weak var parImage: UIImageView!
	@IBOutlet weak var waterField: UILabel!
	@IBOutlet weak var tempField: UILabel!
	@IBOutlet weak var phField: UILabel!
	@IBOutlet weak var condField: UILabel!
	@IBOutlet weak var levelSelect: UISegmentedControl!
	@IBOutlet weak var parLabel: UILabel!
	@IBOutlet weak var parField: UILabel!

	//MARK: - Functions

	override func viewDidLoad() {
		super.viewDidLoad()

		water.attachLabels(unified: waterField, image: waterImage)
		temp.attachLabels(unified: tempField, image: tempImage)
		ph.attachLabels(unified: phField, image: phImage)
		cond.attachLabels(unified: condField, image: condImage)
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		updateSegments()
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		serial.delegate = self

		if let sensor = segueSensor {
			switch sensor {
			case .water:
				performSegue(withIdentifier: "water", sender: self)
			case .temp:
				performSegue(withIdentifier: "temp", sender: self)
			case .ph:
				performSegue(withIdentifier: "ph", sender: self)
			case .cond:
				performSegue(withIdentifier: "cond", sender: self)
			case .par1:
				performSegue(withIdentifier: "par", sender: self)
			case .par2:
				performSegue(withIdentifier: "par", sender: self)
			case .par3:
				performSegue(withIdentifier: "par", sender: self)
			}

		}

	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()

		// Dispose of any resources that can be recreated.
	}

	func updateSegments() {
		for enabledSegment in 0..<levels {
			levelSelect.setEnabled(true, forSegmentAt: enabledSegment)
		}

		for disabledSegment in levels..<levelSelect.numberOfSegments {
			levelSelect.setEnabled(false, forSegmentAt: disabledSegment)
		}

		if levelSelect.selectedSegmentIndex == UISegmentedControlNoSegment {
			parLabel.text = "PAR"
			if levels == 0 {
				parField.text = "No levels connected"
			} else {
				parField.text = "Please select a level"
			}

		}

	}

	func updatePar() {
		switch levelSelect.selectedSegmentIndex {
		case 0:
			parLabel.text = "Level 1 PAR"
			par2.unifiedLabel = nil
			par3.unifiedLabel = nil
			par1.attachLabels(unified: parField, image: parImage)

		case 1:
			parLabel.text = "Level 2 PAR"
			par1.unifiedLabel = nil
			par3.unifiedLabel = nil
			par2.attachLabels(unified: parField, image: parImage)

		case 2:
			parLabel.text = "Level 3 PAR"
			par1.unifiedLabel = nil
			par2.unifiedLabel = nil
			par3.attachLabels(unified: parField, image: parImage)

		default:
			break
		}

	}

	//MARK: - BluetoothSerialDelegate

	func serialDidChangeState() {
		if !serial.isPoweredOn {
			// If Bluetooth was turned off.
		}
	}

	func serialDidDisconnect(_ peripheral: CBPeripheral, error: NSError?) {
		// If peripheral disconnected.
	}

	func serialDidReceiveString(_ message: String) {
		do {
			let _ = try parse(message)
			updateSegments()

		} catch {
			logError(error, with: message)
		}

	}

	//MARK: - NewLevelDelegate

	func newLevelSelected(level: Int) {
		if level + 1 <= levels {
			levelSelect.selectedSegmentIndex = level
			updatePar()
		}

	}

	//MARK: - Navigation

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "par" {
			let newVC = segue.destination as! ParViewController

			if let sensor = segueSensor {
				switch sensor {
				case .par1:
					newVC.selectedLevel = 0
				case .par2:
					newVC.selectedLevel = 1
				case .par3:
					newVC.selectedLevel = 2
				default:
					break
				}

			} else {
				newVC.selectedLevel = levelSelect.selectedSegmentIndex
			}

			newVC.delegate = self
		}

	}

	//MARK: - IBActions

	@IBAction func newLevelSelected(_ sender: UISegmentedControl) {
		updatePar()
	}

	@IBAction func unwindToDetails(unwindSegue: UIStoryboardSegue) {}
	
}
