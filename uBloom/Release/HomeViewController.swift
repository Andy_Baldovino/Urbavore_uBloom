//
//  HomeViewController.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-05-30.
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import UIKit
import CoreBluetooth

class HomeViewController: UIViewController, BluetoothSerialDelegate {

	//MARK: IBOutlets
	
	@IBOutlet weak var image: UIImageView!
	@IBOutlet weak var systemStatusLabel: UILabel!
	@IBOutlet weak var harvestLabel: UILabel!
	@IBOutlet weak var issueButton: UIButton!

	//MARK: - Variables

	var buttonText: String?
	var segue: PossibleSegue?
	var segueToBluetooth = true

	//MARK: - Functions

	override func viewDidLoad() {
		super.viewDidLoad()

		performInitialSetup()

		// Initialize serial.
		serial = BluetoothSerial(delegate: self)
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		updateSystemStatus()
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		serial.delegate = self
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()

		// Dispose of any resources that can be recreated.
	}

	func checkSensor(_ sensor: Sensor) -> Bool {
		guard let status = sensor.status, status != .ok else {
			return false
		}

		buttonText = "\(sensor.description) is \(status.rawValue)"

		return true
	}

	func updateSystemStatus() {
		var issues = 0

		// Reverse order to show the first sensor in Details tab that has an issue.
		if levels >= 3, checkSensor(par3) {
			issues += 1
			segue = .par3
		}
		if levels >= 2, checkSensor(par2) {
			issues += 1
			segue = .par2
		}
		if levels >= 1, checkSensor(par1) {
			issues += 1
			segue = .par1
		}
		if checkSensor(cond) {
			issues += 1
			segue = .cond
		}
		if checkSensor(ph) {
			issues += 1
			segue = .ph
		}
		if checkSensor(temp) {
			issues += 1
			segue = .temp
		}
		if checkSensor(water) {
			issues += 1
			segue = .water
		}

		if serial.isReady {
			switch issues {
			case 0:
				systemStatusLabel.text = "System is healthy."
				image.image = UIImage(named: "plant.ok.png")
//				issueButton.isHidden = true
//				harvestLabel.isHidden = false
				issueButton.isEnabled = false
				issueButton.setTitle("No issues", for: .normal)

			case 1:
				systemStatusLabel.text = "System has 1 issue."
				image.image = UIImage(named: "plant.issues.png")
//				harvestLabel.isHidden = true
//				issueButton.isHidden = false
				issueButton.isEnabled = true
				issueButton.setTitle(buttonText, for: .normal)

			default:
				systemStatusLabel.text = "System has \(issues) issues."
				image.image = UIImage(named: "plant.issues.png")
//				harvestLabel.isHidden = true
//				issueButton.isHidden = false
				issueButton.isEnabled = true
				issueButton.setTitle(buttonText, for: .normal)
			}
			segueToBluetooth = false

		} else {
			systemStatusLabel.text = "System not connected."
			image.image = UIImage(named: "plant.issues.png")
			issueButton.isEnabled = true
			issueButton.setTitle("Select a Device", for: .normal)
			segueToBluetooth = true
		}

	}

	//MARK: - BluetoothSerialDelegate

	func serialDidChangeState() {
		if !serial.isPoweredOn {
			// If Bluetooth was turned off.
			updateSystemStatus()
		}

	}

	func serialDidDisconnect(_ peripheral: CBPeripheral, error: NSError?) {
		// If peripheral disconnected.
		updateSystemStatus()
	}

	func serialDidReceiveString(_ message: String) {
		do {
			let _ = try parse(message)
			updateSystemStatus()

		} catch {
			logError(error, with: message)
		}

	}

	//MARK: - IBActions

	@IBAction func issueButtonPressed(_ sender: UIButton) {
		if segueToBluetooth {
			tabBarController?.selectedIndex = 3

		} else {
			segueSensor = segue
			tabBarController?.selectedIndex = 1
		}

	}

}
