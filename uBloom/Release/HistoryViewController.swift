//
//  HistoryViewController.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-05-30.
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import UIKit
import CoreBluetooth
import Charts

/// Indices of time components in record.
private enum TimeIndex: Int {
	case year = 0, month, week, day
}

/// Indices of sensors in record.
private enum SensorIndex: Int {
	case water = 5, temp, cond, ph, par1, par2, par3
}

class HistoryViewController: UIViewController, BluetoothSerialDelegate {

	//MARK: - IBOutlets

	@IBOutlet weak var timeSelect: UISegmentedControl!
	@IBOutlet weak var sensorSelect: UISegmentedControl!
	@IBOutlet weak var parSelect: UISegmentedControl!
	@IBOutlet weak var plot: LineChartView!

	//MARK: - Variables

	private var timeRange: TimeIndex = .year
	private var sensor: SensorIndex = .water

	//MARK: - Functions

	override func viewDidLoad() {
		super.viewDidLoad()

		plot.chartDescription?.enabled = false
		plot.dragEnabled = false
		plot.setScaleEnabled(true)
		plot.pinchZoomEnabled = false
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		preparePlot()
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		serial.delegate = self
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()

		// Dispose of any resources that can be recreated.
	}

	func preparePlot() {
		do {
			var lineChartEntries = [ChartDataEntry]()

			var startYear = Defaults.year
			if timeRange == .year {
				startYear -= 1
			}

			var startMonth = Defaults.month
			if timeRange == .month {
				if startMonth == 1 {
					startYear -= 1
					startMonth = 12

				} else {
					startMonth -= 1
				}

			}

			var startDay = Defaults.day
			if timeRange == .week {
				if startDay <= 7 {
					if startMonth == 1 {
						startYear -= 1
						startMonth = 12
					} else {
						startMonth -= 1
					}

					startDay += getNumberOfDays(month: startMonth, year: startYear) - 7

				} else {
					startDay -= 7
				}

			}
			if timeRange == .day {
				if startDay == 1 {
					if startMonth == 1 {
						startYear -= 1
						startMonth = 12
					} else {
						startMonth -= 1
					}

					startDay = getNumberOfDays(month: startMonth, year: startYear)

				} else {
					startDay -= 1
				}

			}

			let startHour = Defaults.hour
			let startMinute = Defaults.minute

			let file = try read(from: recordsUrl)
			var records = file.components(separatedBy: "\n")
			guard !records.isEmpty else {
				return
			}
			records.removeLast()

			var averaging = false
			var currentMonth = 0
			var currentDay = 0
			var currentHour = 0
			var currentMinute = 0
			var time = 0.0
			var sum = 0.0
			var count = 0

			gatherData: for record in records {
				let components = record.components(separatedBy: delimiter)
				let year = Int(components[0])!
				let month = Int(components[1])!
				let day = Int(components[2])!
				let hour = Int(components[3])!
				let minute = Int(components[4])!
				let value = Double(components[sensor.rawValue])!

				switch timeRange {
				case .year:
					if !averaging {
						if year < startYear {
							continue gatherData
						}
						if month < startMonth {
							continue gatherData
						}
						if day < startDay {
							continue gatherData
						}

						currentMonth = month
						currentDay = day
						time = 0
						sum = 0
						count = 0
						averaging = true

					}

					if averaging {
						if month == currentMonth, day < currentDay + 4 {
							time += getDouble(year: year, month: month, day: day)
							sum += value
							count += 1

						} else {
							let x = time / Double(count)
							let y = sum / Double(count)
							let dataPoint = ChartDataEntry(x: x, y: y)
							lineChartEntries.append(dataPoint)

							currentMonth = month
							currentDay = day
							time = getDouble(year: year, month: month, day: day)
							sum = value
							count = 1
						}

					}

				case .month:
					if !averaging {
						if year < startYear {
							continue gatherData
						}
						if month < startMonth {
							continue gatherData
						}
						if day < startDay {
							continue gatherData
						}
						if hour < startHour {
							continue gatherData
						}

						currentDay = day
						currentHour = hour
						time = 0
						sum = 0
						count = 0
						averaging = true

					}

					if averaging {
						if day == currentDay, hour < currentHour + 8 {
							time += getDouble(year: year, month: month, day: day, hour: hour)
							sum += value
							count += 1

						} else {
							let x = time / Double(count)
							let y = sum / Double(count)
							let dataPoint = ChartDataEntry(x: x, y: y)
							lineChartEntries.append(dataPoint)

							currentDay = day
							currentHour = hour
							time = getDouble(year: year, month: month, day: day, hour: hour)
							sum = value
							count = 1
						}

					}

				case .week:
					if !averaging {
						if year < startYear {
							continue gatherData
						}
						if month < startMonth {
							continue gatherData
						}
						if day < startDay {
							continue gatherData
						}
						if hour < startHour {
							continue gatherData
						}

						currentDay = day
						currentHour = hour
						time = 0
						sum = 0
						count = 0
						averaging = true

					}

					if averaging {
						if day == currentDay, hour < currentHour + 12 {
							time += getDouble(year: year, month: month, day: day, hour: hour)
							sum += value
							count += 1

						} else {
							let x = time / Double(count)
							let y = sum / Double(count)
							let dataPoint = ChartDataEntry(x: x, y: y)
							lineChartEntries.append(dataPoint)

							currentDay = day
							currentHour = hour
							time = getDouble(year: year, month: month, day: day, hour: hour)
							sum = value
							count = 1
						}

					}

				case .day:
					if !averaging {
						if year < startYear {
							continue gatherData
						}
						if month < startMonth {
							continue gatherData
						}
						if day < startDay {
							continue gatherData
						}
						if hour < startHour {
							continue gatherData
						}
						if minute < startMinute {
							continue gatherData
						}

						currentHour = hour
						currentMinute = minute
						time = 0
						sum = 0
						count = 0
						averaging = true

					}

					if averaging {
						if hour == currentHour, minute < currentMinute + 15 {
							time += getDouble(year: year, month: month, day: day, hour: hour, minute: minute)
							sum += value
							count += 1

						} else {
							let x = time / Double(count)
							let y = sum / Double(count)
							let dataPoint = ChartDataEntry(x: x, y: y)
							lineChartEntries.append(dataPoint)

							currentHour = hour
							currentMinute = minute
							time = getDouble(year: year, month: month, day: day, hour: hour, minute: minute)
							sum = value
							count = 1
						}

					}
				}

			}

			if lineChartEntries.isEmpty {
				plot.data = nil
			} else {
				let line = LineChartDataSet(values: lineChartEntries, label: "Data")
				let gradientColors = [ChartColorTemplates.colorFromString("#0033b5e5").cgColor,
						      ChartColorTemplates.colorFromString("#ff33b5e5").cgColor]
				let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!

				line.fillAlpha = 1
				line.fill = Fill(linearGradient: gradient, angle: 90)
				line.drawFilledEnabled = true
				line.setColor(UIColor(red: 51/255, green: 181/255, blue: 229/255, alpha: 1))
				line.drawValuesEnabled = false
				line.drawCirclesEnabled = false
				line.mode = .horizontalBezier

				let data = LineChartData(dataSet: line)
				data.highlightEnabled = false
				plot.data = data
				plot.animate(xAxisDuration: 3)
			}

		} catch {
			logError(error)
		}

	}

	//MARK: - BluetoothSerialDelegate

	func serialDidChangeState() {
		if !serial.isPoweredOn {
			// If Bluetooth was turned off.
		}
	}

	func serialDidDisconnect(_ peripheral: CBPeripheral, error: NSError?) {
		// If peripheral disconnected.
	}

	func serialDidReceiveString(_ message: String) {
		do {
			let _ = try parse(message)
			preparePlot()

		} catch {
			logError(error, with: message)
		}

	}

	//MARK: - IBActions

	@IBAction func newTimeSelected(_ sender: UISegmentedControl) {
		switch timeSelect.selectedSegmentIndex {
		case 0:
			timeRange = .year
		case 1:
			timeRange = .month
		case 2:
			timeRange = .week
		case 3:
			timeRange = .day
		default:
			break
		}

		preparePlot()
	}

	@IBAction func newSensorSelected(_ sender: UISegmentedControl) {
		parSelect.selectedSegmentIndex = UISegmentedControlNoSegment

		switch sensorSelect.selectedSegmentIndex {
		case 0:
			sensor = .water
		case 1:
			sensor = .temp
		case 2:
			sensor = .ph
		case 3:
			sensor = .cond
		default:
			break
		}

		preparePlot()
	}

	@IBAction func newParSelected(_ sender: UISegmentedControl) {
		sensorSelect.selectedSegmentIndex = UISegmentedControlNoSegment

		switch parSelect.selectedSegmentIndex {
		case 0:
			sensor = .par1
		case 1:
			sensor = .par2
		case 2:
			sensor = .par3
		default:
			break
		}

		preparePlot()
	}

}
