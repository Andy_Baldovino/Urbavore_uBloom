//
//  BluetoothTableViewController.swift
//  uBloom
//
//  Created by Andy Baldovino on 2018-03-12.
//
//  Copyright 2018 Andy Baldovino
//  Licensed under the Apache License, Version 2.0
//

import UIKit
import CoreBluetooth

class BluetoothTableViewController: UITableViewController, BluetoothSerialDelegate {

	//MARK: IBOutlets

	@IBOutlet weak var scanButton: UIButton!

	//MARK: - Variables

	/// The peripherals that have been discovered.
	var discoveredPeripherals: [(peripheral: CBPeripheral, RSSI: Float)] = []

	/// The peripheral the user has selected.
	var selectedPeripheral: CBPeripheral?

	/// Whether scan is occurring.
	var scanning: Bool = false

	/// Whether attempting to connect.
	var connecting: Bool = false

	/// Whether attempt to connect failed.
	var failed: Bool = false

	//MARK: - Functions

	override func viewDidLoad() {
		super.viewDidLoad()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		refreshScanButton()
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		serial.delegate = self
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()

		// Dispose of any resources that can be recreated.
	}

	func refreshScanButton() {
		if serial.isReady {
			scanButton.isEnabled = true
			scanButton.tintColor = UIColor.red
			scanButton.setTitle("Disconnect from \(serial.connectedPeripheral!.name ?? "Device")", for: .normal)

		} else if connecting {
			scanButton.isEnabled = false
			scanButton.tintColor = view.tintColor
			scanButton.setTitle("Connecting to \(selectedPeripheral!.name ?? "Device")…", for: .normal)

		} else if scanning {
			scanButton.isEnabled = true
			scanButton.tintColor = view.tintColor
			scanButton.setTitle("Stop Scanning", for: .normal)

		} else if serial.isPoweredOn {
			scanButton.isEnabled = true
			scanButton.tintColor = view.tintColor
			scanButton.setTitle("Scan for Devices", for: .normal)

		} else {
			scanButton.isEnabled = false
			scanButton.tintColor = view.tintColor
			scanButton.setTitle("Bluetooth Disabled", for: .normal)
		}
	}

	func clearPeripherals() {
		discoveredPeripherals = []
		tableView.reloadData()
	}

	@objc func scanTimedOut() {
		serial.stopScan()
		scanning = false
		refreshScanButton()
	}

	@objc func connectTimedOut() {
		// Ignore if already connected.
		if serial.connectedPeripheral != nil {
			return
		}

		if selectedPeripheral != nil {
			serial.disconnect()
			selectedPeripheral = nil
			connecting = false
			refreshScanButton()
		}
	}

	//MARK: - Table view data source

	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return discoveredPeripherals.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

		// Table view cells are reused and should be dequeued using a cell identifier.
		let cellIdentifier = "PeripheralTableViewCell"
		let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! PeripheralTableViewCell

		cell.peripheralName.text = discoveredPeripherals[(indexPath as NSIndexPath).row].peripheral.name

		return cell
	}

	//MARK: - BluetoothSerialDelegate

	func serialDidChangeState() {
		if !serial.isPoweredOn {
			// If Bluetooth was turned off.
			serial.stopScan()
			scanning = false
			clearPeripherals()
		}

		refreshScanButton()
	}

	func serialDidDisconnect(_ peripheral: CBPeripheral, error: NSError?) {
		// If peripheral disconnected.
		refreshScanButton()
	}

	func serialDidDiscoverPeripheral(_ peripheral: CBPeripheral, RSSI: NSNumber?) {
		// Ignore new peripheral if it has already been added.
		for exisiting in discoveredPeripherals {
			if exisiting.peripheral.identifier == peripheral.identifier {
				return
			}

		}

		// Add new peripheral to the array, then sort and reload array.
		let peripheralRSSI = RSSI?.floatValue ?? 0.0
		discoveredPeripherals.append((peripheral: peripheral, RSSI: peripheralRSSI))
		discoveredPeripherals.sort {
			$0.RSSI < $1.RSSI
		}
		tableView.reloadData()
	}

	func serialDidFailToConnect(_ peripheral: CBPeripheral, error: NSError?) {
		connecting = false
		refreshScanButton()
	}

	func serialIsReady(_ peripheral: CBPeripheral) {
		connecting = false
		refreshScanButton()
	}

	//MARK: - UITableViewDelegate

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		// User has selected a row.

		tableView.deselectRow(at: indexPath, animated: true)

		// Only connect to new peripheral if not already connected/connecting to a peripheral.
		if serial.connectedPeripheral == nil && !connecting {
			serial.stopScan()
			scanning = false
			selectedPeripheral = discoveredPeripherals[(indexPath as NSIndexPath).row].peripheral
			serial.connectToPeripheral(selectedPeripheral!)
			Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(connectTimedOut), userInfo: nil, repeats: false)
			connecting = true
			refreshScanButton()
		}

	}

	//MARK: - IBActions

	@IBAction func scanButtonPressed(_ sender: Any) {
		if serial.connectedPeripheral != nil {
			serial.disconnect()
			refreshScanButton()

		} else if !scanning {
			clearPeripherals()
			serial.startScan()
			Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(scanTimedOut), userInfo: nil, repeats: false)
			scanning = true
			refreshScanButton()

		} else {
			serial.stopScan()
			scanning = false
			refreshScanButton()
		}

	}

}
